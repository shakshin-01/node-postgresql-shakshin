const Router = require('express')
const router = new Router()
const productsRouter = require('./productsRouter')


router.use('/products', productsRouter)

module.exports = router