const uuid = require("uuid");
const path = require("path");
const { Products } = require("../models/models");
const fs = require("fs");
const ApiError = require("../error/ApiError");

class ProductsController {
  async create(req, res, next) {
    try {
      const { name, price, qty } = req.body;
      const { img } = req.files;
      let fileName = uuid.v4() + ".jpg";
      img.mv(path.resolve(__dirname, "..", "static", fileName));

      const products = await Products.create({
        name,
        price,
        qty,
        img: fileName,
      });

      return res.json(products);
    } catch (e) {
      next(ApiError.badRequest(e.message));
    }
  }
  async getAll(req, res) {
    let { limit, page } = req.query;
    page = page || 1;
    limit = limit || 9;
    let offset = page * limit - limit;
    const products = await Products.findAll({ limit, offset });
    return res.json(products);
  }
  async getOne(req, res) {
    const { id } = req.params;
    const products = await Products.findOne({
      where: { id },
    });
    return res.json(products);
  }
  async update(req, res) {
    const { id } = req.params;
    const { name, price, qty } = req.body;
    const { img } = req.files;
    let fileName = uuid.v4() + ".jpg";
    img.mv(path.resolve(__dirname, "..", "static", fileName));
    const products = await Products.update(
      { name: name, price: price, qty: qty, img: fileName },
      { where: { id: id } }
    );
    return res.json(products);
  }

  async delete(req, res, next) {
    try {
      const { id } = req.params;
      const products = await Products.findOne({
        where: { id },
      });
      const fileName = products.img;
      await products.destroy();
      fs.unlink(path.resolve(__dirname, "..", "static", fileName), (err) => {
        if (err) throw err;
        console.log(`${fileName} was deleted`);
      });
      return res.json(products);
    } catch (e) {
      next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new ProductsController();
