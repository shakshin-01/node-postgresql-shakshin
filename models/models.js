const sequelize = require('../db')
const {DataTypes} = require('sequelize')


const Products = sequelize.define('products', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, unique: true, allowNull: false},
    price: {type: DataTypes.INTEGER, allowNull: false},
    qty: {type: DataTypes.INTEGER},
    img: {type: DataTypes.STRING, allowNull: false},
})



module.exports = { Products }
